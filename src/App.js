import './App.css';
import React, {useEffect, useState} from "react";
import AppRoutes from './Components/AppRoutes';
import {useSelector, useDispatch} from 'react-redux';
import {addToItem} from './assets/AllSlice/itemSlice';
import {effectStar} from './assets/AllSlice/starListSlice';
import {effectOder} from './assets/AllSlice/oderSlice';
import {fetchTodos} from './assets/AllSlice/itemSlice';
import Modal from "./Components/Modal";
import {Context} from "./Components/Context";

function App() {

    const dispatch = useDispatch()
    const selectorStar = useSelector(state => state.star.star)
    const selectorOders = useSelector(state => state.oder.oder)
    const selectorItem = useSelector(state => state.item.item)

    useEffect(() => {
        dispatch(effectOder(JSON.parse(localStorage.getItem(`selectorOders`)) || []))
    }, [])

    useEffect(() => {
        localStorage.setItem(`selectorOders`, JSON.stringify(selectorOders))
    }, [selectorOders])


    useEffect(() => {
        dispatch(effectStar(JSON.parse(localStorage.getItem(`Selectorstar`)) || []))
    }, [])

    useEffect(() => {
        localStorage.setItem(`Selectorstar`, JSON.stringify(selectorStar))
    }, [selectorStar])
    useEffect(() => {

        dispatch(fetchTodos());
    }, []);

    return (
        <>
            {
                <Context>
                    <div className="App">
                        <AppRoutes/>
                    </div>
                </Context>
            }
        </>
    );
}

export default App;
