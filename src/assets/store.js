import { configureStore } from '@reduxjs/toolkit';
import itemReducer from './AllSlice/itemSlice';
import starListSlice from './AllSlice/starListSlice';
import BeforOder from "./AllSlice/addToBeforOder";
import Oder from "./AllSlice/oderSlice";
import deleteOder from './AllSlice/odersDelete';
import isOpenApprove from './AllSlice/TogleAproveModal';
import ModalDelToogle from "./AllSlice/ModalDelToogle";
import FormSlice from './AllSlice/FromSlice'

const store = configureStore({
    reducer: {
        item: itemReducer,
        star: starListSlice,
        beforOder: BeforOder,
        oder: Oder,
        deleteOder: deleteOder,
        isOpenApprove : isOpenApprove,
        isOpenDel : ModalDelToogle,
        FormSlice: FormSlice,
    },
});

export default store