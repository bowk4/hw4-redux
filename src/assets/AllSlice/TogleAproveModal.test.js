import TogleAproveModal, {modalToogleApprove} from "./TogleAproveModal";

describe(`test TogleAproveModal`, () => {
    it(`TogleAproveModal testing first`, () => {
        const payload = `Test`
        const action = modalToogleApprove(payload)
        const initialstate =  {
            isOpenApprove : null
        }
        const TogleAproveModalTest = TogleAproveModal(initialstate  ,action)
        expect(TogleAproveModalTest.isOpenApprove).toEqual(payload)
    })
})