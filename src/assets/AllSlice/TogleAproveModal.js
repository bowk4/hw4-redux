import { createSlice } from "@reduxjs/toolkit"
const TogleAproveModal = createSlice( {
    name: `TogleAproveModal` ,
    initialState: {
        isOpenApprove : null
    },
    reducers:{
        modalToogleApprove(state , action){
            state.isOpenApprove = action.payload
        },
    }
})
export const {modalToogleApprove} = TogleAproveModal.actions
export default TogleAproveModal.reducer