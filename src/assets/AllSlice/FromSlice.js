import { createSlice } from "@reduxjs/toolkit"

const FormSlice = createSlice({
    name: `star` ,
    initialState: {
        messageForm : false
    } ,
    reducers: {
        toogleMessageForm(state  ){
            state.messageForm = !state.messageForm

        },

    }
})
export const {toogleMessageForm } =  FormSlice.actions;
export default FormSlice.reducer;