import {createSlice} from "@reduxjs/toolkit"

const oderSlice = createSlice({
    name: `oder`,
    initialState: {
        oder: []
    },
    reducers: {
        addOder(state, action) {
            let approve = false
            state.oder.forEach((el) => {
                if (el.id === action.payload.id) {
                    approve = true;
                    el.count = el.count + 1

                }
            })
            !approve && state.oder.push({...action.payload, idTime: new Date().toISOString(), count: 1})

        },
        deleteFromOder(state, action) {

            state.oder.forEach((el) => {
                if (el.id === action.payload.id) {
                    el.count === 1 ? state.oder = state.oder.filter((el) => el.id !== action.payload.id) : el.count = el.count - 1
                }
            })

        },
        effectOder(state, action) {
            state.oder = action.payload;

        },
        incrementCount(state, action){
            state.oder.forEach((el) => {
                if (el.id === action.payload.id) {

                    el.count = el.count + 1

                }
            })
        },
        decrementCount(state, action){
            state.oder.forEach((el) => {
                if (el.id === action.payload.id) {
                    if(el.count > 1){

                        el.count = el.count - 1
                    }
                }
            })
        },
        deleteAllOders(state){

            state.oder = []
        }
    }
})
export const {addOder, deleteFromOder, effectOder , deleteAllOders ,incrementCount ,decrementCount} = oderSlice.actions;
export default oderSlice.reducer;