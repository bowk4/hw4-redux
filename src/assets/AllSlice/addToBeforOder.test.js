import addToBeforOder, {addTobeforOder} from "./addToBeforOder";

describe(`test reducer addToBeforOder`, () => {
    it(`addToBeforOder testing first`, () => {
        const payload = {counter: 1}
        const action = addTobeforOder(payload)
        const initialstate = {
            beforOder: []
        }
        const newReducer = addToBeforOder(initialstate , action)
        expect(newReducer.beforOder).toEqual({counter: 1})
    })
})