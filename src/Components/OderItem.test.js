import {render} from "@testing-library/react";
import OdersItem from "./OdersItem";
import {Provider} from "react-redux";
import store from "../assets/store";

it(`OderItem test (Snapshot)`, () => {
    const item = {
        "id": 1,
        "name": "Стілець",
        "img": "chair-grey.jpeg",
        "article": "58367",
        "color": "Gray",
        "price": 49.99,
        "count": 1
    }

    const oder = render(<Provider store={store}>
        <OdersItem item={item}/></Provider>)
    expect(oder).toMatchSnapshot()
})