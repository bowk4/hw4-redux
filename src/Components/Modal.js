import React from 'react';
import PropTypes from 'prop-types';
import Button from './Button'

import {addOder, deleteFromOder} from '../assets/AllSlice/oderSlice';
import {useSelector, useDispatch} from 'react-redux';
import {modalToogleApprove} from '../assets/AllSlice/TogleAproveModal';
import {ToogleModalDel} from "../assets/AllSlice/ModalDelToogle";

function Modal({Modal_text, titleName, isModalFor}) {
    const selectorBeforOder = useSelector(state => state.beforOder.beforOder)

    const selectdeleteOder = useSelector(state => state.deleteOder.deleteOder)
    const dispatch = useDispatch();

    return (
        <>
            <div id="blur" className="blur" onClick={(e) => (
                e.target.id === "blur" && (isModalFor === "Approve" ? dispatch(modalToogleApprove()) : dispatch(ToogleModalDel()))
            )}>

                <div className="modal">
                    <h2 className="modal__title">{titleName}</h2>
                    <p>{Modal_text}</p>

                    {isModalFor === "Approve" && <div className="modal__btn-wrapper">
                        <Button
                            Click={() => {
                                dispatch(addOder(selectorBeforOder));
                                dispatch(modalToogleApprove());

                            }}
                            Text={'Ok'}
                        />
                        <Button Click={() => {
                            dispatch(modalToogleApprove())
                        }} Text={'Cancel'}/>
                    </div>
                    }
                    {isModalFor === "Delete" && <div className="modal__btn-wrapper">
                        <Button
                            Click={() => {
                                dispatch(deleteFromOder(selectdeleteOder))
                                dispatch(ToogleModalDel())
                            }} Text={'Ok'}
                        />
                        <Button
                            Click={() => {
                                dispatch(ToogleModalDel())
                            }}
                            Text={'Cancel'}
                        />
                    </div>
                    }
                </div>
            </div>
        </>
    );
}

Modal.propTypes = {
    Modal_text: PropTypes.string,
    titleName: PropTypes.string,
}

export default Modal;