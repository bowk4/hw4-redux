import React, {useState} from 'react';

export const MyCreateContext = React.createContext({isTable: false});
export const Context = ({children}) => {
    const [isTable , setIsTable] =  useState(false)
    const falseTable = ()=>{
        setIsTable(false)
    }
    const trueTable = ()=>{
        setIsTable(true)
    }
    const  value = {falseTable , trueTable , isTable}
    return <MyCreateContext.Provider value={value}>{children}</MyCreateContext.Provider>;
};
