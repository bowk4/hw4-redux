import React, {useState} from 'react'
import {Route, Routes} from 'react-router-dom';
import {Oders} from '../Pages/Oders';
import {Star} from '../Pages/Star';
import {Card} from "../Pages/Card";
import {Header} from "./Header";
import {Form} from "./Form";


const AppRoutes = () => {

    return (
        <>
            <Routes>
                <Route path='/' element={<Header/>}>
                    <Route index path='/' element={<Card/>}/>
                    <Route path='/Shop' element={<Oders/>}/>
                    <Route path='/Star' element={<Star/>}/>
                    <Route path='/Form' element={<Form/>}/>
                </Route>
            </Routes>
        </>
    )
}

export default AppRoutes