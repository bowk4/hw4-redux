import React, {useState} from 'react';
import {useFormik} from "formik";
import * as Yup from 'yup'
import {deleteAllOders} from "../assets/AllSlice/oderSlice";
import {useDispatch} from "react-redux";
import {useNavigate} from "react-router-dom";
import {useSelector} from "react-redux";
import {toogleMessageForm} from '../assets/AllSlice/FromSlice'

const Form = () => {
    const [Load, setLoad] = useState(false)
    const [showModal, setShowModal] = useState(false);

    const selectOder = useSelector(state => state.oder.oder)
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const valid = Yup.object().shape({
        name: Yup.string().required(`Введіть сюда відповідні дані`).matches(/^[\p{L}]+$/u, 'Мають бути лише літери'),
        surname: Yup.string().required(`Введіть сюда відповідні дані`).matches(/^[\p{L}]+$/u, 'Мають бути лише літери'),
        age: Yup.number().typeError('Дозволяються лише цифри').required(`Введіть сюда відповідні дані`).positive(`Вік не може бути негативним`),
        address: Yup.string().required(`Введіть сюда відповідні дані`),
        mobile: Yup.number().typeError('Дозволяються лише цифри').required('Введіть сюда відповідні дані')
    })

    const formik = useFormik(
        {
            initialValues: {
                name: '',
                surname: '',
                age: '',
                address: '',
                mobile: '',
            },

            onSubmit: values => {
                setLoad(true)
                setTimeout(() => {
                    setLoad(false)
                    console.log(`values`, values)
                    console.log(`selectOder`, selectOder)
                    dispatch(deleteAllOders())
                    formik.resetForm()
                    navigate("/");
                    dispatch(toogleMessageForm())
                    setShowModal(true)

                    setTimeout(() => {
                        setShowModal(false);
                        dispatch(toogleMessageForm())
                    }, 7000)
                }, 5000)
            },
            validationSchema: valid,
        }
    )

    return (
        <div className='warpper-form'>
            <form onSubmit={formik.handleSubmit}>
                <h2>Введіть Ваші дані</h2>
                <div className={'wrapper-input'}>
                    <label htmlFor="name">
                        <p className="form-label-text">Ваше ім'я:</p>
                        <input
                            name={'name'}
                            id="name"
                            placeholder={`Ім'я`}
                            onChange={formik.handleChange}
                            value={formik.values.name}
                        />
                    </label>
                    {formik.errors.name && formik.touched.name && <p className="text-error">{formik.errors.name}</p>}
                </div>
                <div className={'wrapper-input'}>
                    <label htmlFor={'surname'}>
                        <p className="form-label-text">Ваше прізвище:</p>
                        <input
                            name={'surname'}
                            id={'surname'}
                            placeholder={`Прізвище`}
                            value={formik.values.surname}
                            onChange={formik.handleChange}
                        />
                    </label>
                    {formik.errors.surname && formik.touched.surname &&
                        <p className="text-error">{formik.errors.surname}</p>}
                </div>
                <div className={'wrapper-input'}>
                    <label htmlFor={'age'}>
                        <p className="form-label-text">Ваш вік:</p>
                        <input
                            name={'age'}
                            id={'age'}
                            placeholder={`Вік`}
                            onChange={formik.handleChange}
                            value={formik.values.age}
                        />
                    </label>

                    {formik.errors.age && formik.touched.age && <p className="text-error">{formik.errors.age}</p>}
                </div>
                <div className={'wrapper-input'}>
                    <label htmlFor={'address'}>
                        <p className="form-label-text">Адреса доставки:</p>
                        <input
                            name={'address'}
                            id={'address'}
                            placeholder={`Адреса`}
                            onChange={formik.handleChange}
                            value={formik.values.address}
                        />
                    </label>
                    {formik.errors.address && formik.touched.address &&
                        <p className="text-error">{formik.errors.address}</p>}
                </div>
                <div className={'wrapper-input'}>
                    <label htmlFor={'mobile'}>
                        <p className="form-label-text">Ваш номер телефону:</p>
                        <input
                            className={'input'}
                            name={'mobile'}
                            id={'mobile'}
                            placeholder={`Номер телефону`}
                            onChange={formik.handleChange}
                            value={formik.values.mobile}
                        />
                    </label>
                    {formik.errors.mobile && formik.touched.mobile &&
                        <p className="text-error">{formik.errors.mobile}</p>}
                </div>
                <button type='submit'>Оформити замовення</button>
            </form>
            {Load && <div className="blur">
                <div className="lds-roller">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>}
            {showModal && (
                <div className="modal">
                    <p>Ваш товар оформлено</p>
                </div>
            )}
        </div>
    );
};

export {Form};