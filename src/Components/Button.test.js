import {screen , render , fireEvent} from "@testing-library/react";
import Button from "./Button";

describe(`Button all tests` , ()=>{
    it(`test for click`, ()=>{
        const handelClick =  jest.fn()
        const btnText = `I am Button`
        render(<Button Click={handelClick}  Text={btnText}/>)
        const btn = screen.getByText(btnText)
        fireEvent.click(btn)
        expect(handelClick).toHaveBeenCalledTimes(1)
    })
})