import React, { useState } from 'react';
import { MdOutlineShoppingCart } from "react-icons/md";

import { FaRegStar } from "react-icons/fa";

import PropTypes from 'prop-types';
import { Link, Outlet  , } from 'react-router-dom';
import { useSelector } from 'react-redux';
function Header() {

    const selectorStar = useSelector(state => state.star.star)
    const selectOder = useSelector(state => state.oder.oder);
  
    return (
        <>
            <div className="header">
                <span className="logo"><Link  to="/">
                    <img className="logo-img" src="./logo-img/vital_logo_small.png"  alt="logo"/>
                </Link></span>
                <ul >
                    <p className='oder_counter'>{selectOder.length}</p>

                    <Link to="/Shop"> <MdOutlineShoppingCart className={` icon-buy `}/></Link>
                 
                    <p className='star_counter'> {selectorStar.length}</p>
                    <Link to="/Star"> <FaRegStar  className={` icon-buy `}/></Link>

                    <li><Link to="/">На головну</Link></li>
                    <li>Про нас</li>
                    <li>Контакти</li>
                </ul>
               
            </div>
            <main>
                <Outlet />
            </main>
        </>
    );
}

export { Header};