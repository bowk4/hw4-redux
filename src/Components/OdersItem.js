import React, {useState, useEffect} from 'react'
import {MdDeleteOutline} from "react-icons/md";
import {FaRegStar} from "react-icons/fa";
import {odersDeleteFun} from '../assets/AllSlice/odersDelete';
import {deleteFromStar, addToStar} from '../assets/AllSlice/starListSlice';

import {useDispatch, useSelector} from 'react-redux';
import {ToogleModalDel} from "../assets/AllSlice/ModalDelToogle";
import PropTypes from "prop-types";
import Modal from "./Modal";

const OdersItem = ({item}) => {
    const [addStar, setaddStar] = useState(false)
    const dispatch = useDispatch()
    const deleteModal = useSelector(state => state.isOpenDel.isModalDelOpen)
    useEffect(() => {
        setTimeout(()=>{
            const starData = JSON.parse(localStorage.getItem('Selectorstar'));
            if (starData) {
                starData.forEach((element) => {
                    if (element.id === item.id) {
                        change();
                        // console.log(element)
                    }
                });
            }
        } , 0.1)
    }, []);

    const change = () => {
        setaddStar(!addStar)
    }

    useEffect(() => {
        const starData = JSON.parse(localStorage.getItem('Selectorstar'));
        if (starData) {
            starData.forEach((element) => {
                if (element.idTime === item.idTime) {
                    change();
                }
            });
        }
    }, []);


    return (
        <>
            <div>
                {
                    <div className="buy-item">
                        <img src={"images/" + item.img} alt="oders"/>
                        <h2>{item.name}</h2>
                        <p>Колір: {item.color}</p>
                        <b>Кількість : {item.count} </b>
                        <b>Ціна : {item.count * item.price} $</b>
                        <section>
                            <FaRegStar className={`star_shop ${(addStar) && `star-active`}`} onClick={() => {
                                change();
                                !addStar ? dispatch(addToStar(item)) : dispatch(deleteFromStar(item))
                            }}/>
                            <MdDeleteOutline className='delete' onClick={() => {
                                dispatch(odersDeleteFun(item))
                                dispatch(ToogleModalDel(item.id))
                            }}/>
                        </section>
                    </div>
                }
                {deleteModal === item.id && <Modal
                    Modal_text="Видалити товар з кошику?"
                    titleName="Увага!"
                    isModalFor={"Delete"}
                />}
            </div>
        </>
    )
}

OdersItem.propTypes = {
    item : PropTypes.object.isRequired,
}
export default OdersItem