import React, {useState} from 'react'

const Button = ({Click, Text}) => {

    return (
        <>
            {
                <button className="add_card"
                    onClick={() => {
                        Click()
                    }}>{Text}
                </button>
            }
        </>
    )
}

export default Button