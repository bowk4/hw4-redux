import {Form} from "./Form"
import {render} from "@testing-library/react";
import {Provider} from "react-redux";
import store from "../assets/store";
import {BrowserRouter} from "react-router-dom";
import React from "react"

it(`test form`, () => {
    const form = render(
        <Provider store={store}>
            <BrowserRouter>
                <Form/>
            </BrowserRouter>
        </Provider>)
    expect(form).toMatchSnapshot()
})