import CardItem from "../Components/CardItem";
import React, {useContext} from "react";
import { useSelector } from "react-redux";
import {FaTableList} from "react-icons/fa6";
import {BsTable} from "react-icons/bs";
import {MyCreateContext} from "../Components/Context";

function Card() {
    const selector = useSelector((state) => state.item.item);
    const {falseTable, trueTable, isTable} = useContext(MyCreateContext)

    return (
        <>
            <div className="list-or-table">
                <BsTable style={{width: '35px', height: '35px', margin: '0 25px', color: isTable ? `black` : `darkorange`}} onClick={() => {
                    falseTable()}}></BsTable>
                <FaTableList style={{width: '35px', height: '40px', margin: '0 25px', color: isTable ? `darkorange` : `black` }} onClick={() => {
                    trueTable()}}></FaTableList>
            </div>
            <div className={ isTable ? `card-row`: `card`} style={{flexDirection: isTable ? `column` : `row`}}>
                {selector.map((el) => (
                    <CardItem key={el.id} itemCard={el}/>
                ))}
            </div>
        </>
    );
}

export {Card};
