import OdersItem from '../Components/OdersItem';
import {useSelector} from 'react-redux';
import {Link} from "react-router-dom";

function Oders() {
    const selectOder = useSelector(state => state.oder.oder);
    let allPrice = 0;
    selectOder.forEach((el) => {
        allPrice += (el.price) * el.count
    })

    return (
        <>
            <h2 className="me_shop">Мій кошик</h2>
            { selectOder.length > 0 &&
                <div className='btn-buy_order-wrapper'>
                    <p>Загальна сума: {allPrice} $</p>
                    <Link className="link-to-form" to="/Form">Оформити замовлення</Link>
                </div>
            }
            <div className='Oders-wrapper'>
                {selectOder.length > 0 ?
                    selectOder.map((el) => (
                        <OdersItem
                            key={el.id}
                            item={el}
                        />
                    )) : <div className='Nothing'> Did you chose something? No.</div>
                }
            </div>
        </>
    );
}

export {Oders};