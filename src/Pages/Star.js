import React from 'react';

import { FaRegStar } from "react-icons/fa";
import { useSelector , useDispatch } from 'react-redux';
import { deleteFromStar } from '../assets/AllSlice/starListSlice';
function Star() {
    const stars = useSelector(state => state.star.star)
    const dispatch = useDispatch()
    return (
        <>
            <h2 className="me_star">Мої збереження</h2>
            <div className='Oders-wrapper'>
                {
                    stars.length > 0 ?
                        stars.map((el) => (
                            <div className="buy-item" key={el.id}>
                                <img src={"images/" + el.img} alt="oders"/>
                                <h2>{el.name}</h2>
                                <p>Колір: {el.color}</p>
                                <p>Код товару: {el.article}</p>
                                <b>Ціна: {el.price} $</b>
                                <FaRegStar className='Star-delete' style={{color: 'orange'}} onClick={() => {
                                    dispatch(deleteFromStar(el))
                                }}/>
                            </div>
                        )) : <div className='Nothing'> Did you add something? No.</div>
                }
            </div>
        </>
    );
}

export {Star};